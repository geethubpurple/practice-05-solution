var HORSESHOE_COUNT = 2, MAX_NUMBER = 75;

function cutRandomElement(list) {
    return list.splice(randomNumber(0, list.length - 1), 1)[0];
}

function randomNumber(from, to) {
    return Math.round(Math.random() * (to - from) + from);
}

function keys(list) {
    return Object.keys(list).map(Number);
}

var tickets = {}, playerIds = keys(players);
for (var ticketId = 1; ticketId <= playerIds.length; ticketId++) {
    var ticket = tickets[ticketId] = [];
    for (var fieldId = 0; fieldId < 3; fieldId++) {
        var field = [];
        ticket.push(field);

        var horseShoes = [];

        for (var i = 0; i < HORSESHOE_COUNT; i++) {
            do {
                var horseShoe = randomNumber(0, 24);
            } while (horseShoes.some(function (item) {
                return item % 5 == horseShoe % 5;
            }));

            horseShoes.push(horseShoe);
        }

        var index = 0, list, random;
        for (var col = 0; col < 5; col++) {
            list = [];
            for (var i = 0; i < 5; i++) {
                do {
                    random = randomNumber(col * 15 + 1, (col + 1) * 15 - 1);
                } while (list.some(function (item) {
                    return item === random;
                }));
                list.push(random);
            }

            for (var row = 0; row < 5; row++) {
                if (col == 0) {
                    field.push([]);
                }
                field[row].push(horseShoes.some(function (item) {
                    return item === index;
                }) ? 'HORSESHOE' : cutRandomElement(list));

                index++;
            }
        }

        var checker = {};
        for (var row = 0; row < 5; row++) {
            for (var col = 0; col < 5; col++) {
                checker[field[row][col]] = '';
            }
        }

        if (Object.keys(checker).length != 24) {
            throw 'a-a-a';
        }
    }
}

console.log(JSON.stringify(tickets));