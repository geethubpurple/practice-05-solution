var MAX_NUMBER = 75, HORSESHOE = 'HORSESHOE';

function cutRandomElement(list) {
    return list.splice(randomNumber(0, list.length - 1), 1)[0];
}

function randomNumber(from, to) {
    return Math.round(Math.random() * (to - from) + from);
}

function keys(list) {
    return Object.keys(list).map(Number);
}

var ticketIds = keys(tickets);

var playersTickets = keys(players).map(function (playerId) {
    var ticketId = cutRandomElement(ticketIds), ticket = tickets[ticketId], game = [];

    for (var field = 0; field < 3; field++) {
        game.push([]);
        for (var row = 0; row < 5; row++) {
            game[field].push([]);
            for (var col = 0; col < 5; col++) {
                if (ticket[field][row][col] != HORSESHOE) {
                    game[field][row].push(ticket[field][row][col]);
                }
            }
        }
    }

    return {
        playerId: playerId,
        ticketId: ticketId,
        game: game
    };
});

var balls = [];
for (var i = 0; i < MAX_NUMBER; i++) {
    balls.push(i + 1);
}

// helpers
function printWinner(winner) {
    console.groupCollapsed(players[winner.playerId]);

    var ticket = tickets[winner.ticketId];
    ticket.forEach(function (field, fieldNumber) {
        field.forEach(function (row, rowNumber) {
            console.log(row.map(function (number) {
                    return number === HORSESHOE ? '\u265E' : number;
                }).join('  ') + (winner.game[fieldNumber][rowNumber].length == 0 ? '   \u2190' : ''));
        });
        console.log('');
    });

    console.groupEnd(players[winner.playerId]);
}

function rowDoesNotContainHorseShoe(row) {
    return !row.some(function (item) {
        return item == HORSESHOE;
    });
}

function emptyRow(row) {
    return !row.length;
}

function threeRowsWithHorseShoe(playerTicket, horseShoesAllowed) {
    var ticket = tickets[playerTicket.ticketId];
    return playerTicket.game.filter(function (field, fieldNumber) {
        return field.filter(emptyRow).length == 3 && field.filter(function (row, rowNumber) {
                return !row.length && rowDoesNotContainHorseShoe(ticket[fieldNumber][rowNumber]);
            }).length == 3 - horseShoesAllowed;
    }).length;
}

// game!
var firstRowCrossed = false, playedBalls = [], threeRowsWinners = [];
do {
    var ball = cutRandomElement(balls);
    playedBalls.push(ball);

    // Remove crossed number
    playersTickets.forEach(function (playerTicket) {
        playerTicket.game.forEach(function (field) {
            for (var i = 0; i < 5; i++) {
                field[i] = field[i].filter(function (number) {
                    return number != ball;
                });
            }
        });
    });

    // looking for somebody who crossed first row
    if (!firstRowCrossed && playedBalls.length >= 3) {
        var firstRowsWinners = playersTickets.filter(function (playerTicket) {
            return playerTicket.game.filter(function (field) {
                return field.some(function (row) {
                    return !row.length;
                });
            }).length;
        });

        if (firstRowsWinners.length > 0) {
            console.group('First row crossed!');
            console.log('Balls (' + playedBalls.length + '): ' + playedBalls.join(', '));
            firstRowsWinners.forEach(printWinner);
            console.groupEnd('First row crossed!');
            firstRowCrossed = true;
        }
    }
} while (balls.length > 0 && (threeRowsWinners = playersTickets.filter(function (playerTicket) {
    return playerTicket.game.filter(function (field) {
        return field.filter(function (row) {
                return !row.length;
            }).length == 3;
    }).length;
})).length == 0);

console.log('\nBalls (' + playedBalls.length + '): ' + playedBalls.join(', '));

var losers = playersTickets.slice(0);
[
    {
        name: 'Jackpot',
        checker: function (playerTicket) {
            return threeRowsWithHorseShoe(playerTicket, 0);
        }
    },
    {
        name: 'First Category',
        checker: function (playerTicket) {
            return threeRowsWithHorseShoe(playerTicket, 1);
        }
    },
    {
        name: 'Second Category',
        checker: function (playerTicket) {
            return threeRowsWithHorseShoe(playerTicket, 2);
        }
    },
    {
        name: 'Third Category',
        checker: function (playerTicket) {
            return playerTicket.game.filter(function (field) {
                    return field.filter(emptyRow).length == 2;
                }).length > 0;
        }
    },
    {
        name: 'Forth Category',
        checker: function (playerTicket) {
            return playerTicket.game.filter(function (field) {
                    return field.filter(emptyRow).length == 1;
                }).length > 1;
        }
    },
    {
        name: 'Fifth Category',
        checker: function (playerTicket) {
            return playerTicket.game.filter(function (field) {
                    return field.filter(emptyRow).length == 1;
                }).length > 0;
        }
    }
].forEach(function (price) {
    var winners = losers.filter(price.checker);
    if (winners.length) {
        console.group(price.name + '!');
        winners.forEach(printWinner);
        console.groupEnd(price.name + '!');
        losers = losers.filter(function (looser) {
            return winners.every(function (winner) {
                return winner != looser
            });
        });
    }
});

console.group('Losers!');
losers.forEach(printWinner);
console.groupEnd('Losers!');